var gulp = gulp = require('gulp');

// Include plugins
var sass = sass = require('gulp-sass');

// Compile Our Sass
gulp.task('gnome-shell', function() {
	return gulp.src('sass/gnome-shell/gnome-shell.scss')
		.pipe(sass({
			style: 'expanded'
		}))
		.pipe(gulp.dest('gnome-shell'))
});

gulp.task('gtk', function() {
	return gulp.src('sass/gtk/gtk*.scss')
		.pipe(sass({
			style: 'expanded'
		}))
		.pipe(gulp.dest('gtk-3.0'))
});

gulp.task('dev', function() {
	gulp.watch('sass/gnome-shell/*.scss', ['gnome-shell']);
	gulp.watch('sass/gnome-shell/gnome-shell-sass/*.scss', ['gnome-shell']);
	gulp.watch('sass/gtk/*.scss', ['gtk']);
	gulp.watch('sass/gtk/gtk-sass/*.scss', ['gtk']);
	gulp.watch('sass/globals.scss', ['gnome-shell', 'gtk']);
});
